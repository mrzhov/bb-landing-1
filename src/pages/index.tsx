import type { NextPage } from 'next';

import About from '@/components/About';
import Clients from '@/components/Clients';
import Contacts from '@/components/Contacts';
import Cta from '@/components/Cta';
import Footer from '@/components/Footer';
import Header from '@/components/Header';
import HomeSection from '@/components/HomeSection';
import Services from '@/components/Services';

const Home: NextPage = () => {
	return (
		<>
			<Header />
			<main id='main'>
				<HomeSection />
				<About />
				<Services />
				<Cta />
				<Clients />
				<Contacts />
				<Footer />
			</main>
			<a href='#' className='scroll-top btn-hover'>
				<i className='lni lni-chevron-up'></i>
			</a>
		</>
	);
};

export default Home;
