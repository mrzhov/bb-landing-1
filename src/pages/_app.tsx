import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import '@/styles/globals.css';

import type { AppProps } from 'next/app';
import Head from 'next/head';
import { useEffect } from 'react';

import content from '@/content/app.json';
import { main } from '@/utils/main';

function MyApp({ Component, pageProps }: AppProps) {
	if (process.browser) {
		useEffect(() => {
			main();
		}, []);
	}

	return (
		<>
			<Head>
				<meta name='viewport' content='width=device-width, initial-scale=1' />
				<title>{content.title}</title>
				<meta name='description' content={content.description} />
			</Head>
			<Component {...pageProps} />
		</>
	);
}

export default MyApp;
