// eslint-disable-next-line import/prefer-default-export
export const main = () => {
	window.onscroll = () => {
		const headerNavbar = document.querySelector('.navbar-area');
		const sticky = headerNavbar.offsetTop;

		if (window.pageYOffset > sticky) {
			headerNavbar.classList.add('sticky');
		} else {
			headerNavbar.classList.remove('sticky');
		}

		const backToTop = document.querySelector('.scroll-top');
		if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
			backToTop.style.display = 'flex';
		} else {
			backToTop.style.display = 'none';
		}
	};

	function onScroll() {
		const sections = document.querySelectorAll('.page-scroll');
		const scrollPos =
			window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;

		// eslint-disable-next-line no-plusplus
		for (let i = 0; i < sections.length; i++) {
			const currLink = sections[i];
			const val = currLink.getAttribute('href');
			const refElement = document.querySelector(val);
			const scrollTopMinus = scrollPos + 100;
			if (
				refElement.offsetTop <= scrollTopMinus &&
				refElement.offsetTop + refElement.offsetHeight > scrollTopMinus
			) {
				document.querySelector('.page-scroll').classList.remove('active');
				currLink.classList.add('active');
			} else {
				currLink.classList.remove('active');
			}
		}
	}

	window.document.addEventListener('scroll', onScroll);

	const pageLink = document.querySelectorAll('.page-scroll, .page-scroll-btn');

	pageLink.forEach((elem) => {
		elem.addEventListener('click', (e) => {
			e.preventDefault();
			document.querySelector(elem.getAttribute('href')).scrollIntoView({
				behavior: 'smooth',
				offsetTop: 1 - 60,
			});
		});
	});
};
