import React from 'react';

import content from '@/content/footer.json';

const Footer = (): React.ReactElement => {
	return (
		<footer id='footer'>
			<div className='container'>
				<div className='copyright'>
					&copy; Авторские права{' '}
					<strong>
						<span>{content.companyName}</span>
					</strong>
					. Все права защищены
				</div>
			</div>
		</footer>
	);
};

export default Footer;
