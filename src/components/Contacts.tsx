import { Form, FormikProvider, useFormik } from 'formik';
import type { FC } from 'react';
import React, { useState } from 'react';
import * as Yup from 'yup';

import content from '@/content/contacts.json';

interface Fields {
	name: string;
	email: string;
	message: string;
}

const Contacts: FC = () => {
	const [showFormSuccess, setShowFormSuccess] = useState(false);

	const ContactsSchema = Yup.object().shape({
		name: Yup.string().required('Поле является обязательным'),
		email: Yup.string().email('Почта введена некорректно').required('Поле является обязательным'),
		message: Yup.string().required('Поле является обязательным'),
	});

	const formik = useFormik<Fields>({
		initialValues: {
			name: '',
			email: '',
			message: '',
		},
		validationSchema: ContactsSchema,
		onSubmit: (values) => sendForm(values),
	});

	const { handleSubmit, getFieldProps, errors, resetForm } = formik;

	const hideFormSuccess = () => {
		setShowFormSuccess(false);
		// @ts-ignore
		clearTimeout(hideFormSuccess);
	};

	async function sendForm(fields: Fields) {
		const { name, email, message } = fields;
		const data = new FormData();
		data.append('name', JSON.stringify(name));
		data.append('email', JSON.stringify(email));
		data.append('message', JSON.stringify(message));
		const requestOptions = {
			method: 'post',
			body: data,
		};
		const res = await fetch('/api/message', requestOptions);
		if (res.status === 200) {
			setShowFormSuccess(true);
			setTimeout(hideFormSuccess, 4000);
			resetForm();
		}
	}

	return (
		<section id='contacts' className='contact-section'>
			<div className='container'>
				<div className='section-title-five'>
					<div className='container'>
						<div className='row'>
							<div className='col-12'>
								<div className='content'>
									<h6>{content.sectionName}</h6>
									<h2 className='fw-bold'>{content.title}</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className='row'>
					<div className='col-xl-4'>
						<div className='contact-item-wrapper'>
							<div className='row'>
								<div className='col-12 col-md-6 col-xl-12'>
									<div className='contact-item'>
										<div className='contact-icon'>
											<i className='lni lni-map-marker'></i>
										</div>
										<div className='contact-content'>
											<h4>Адрес</h4>
											<p>{content.address}</p>
										</div>
									</div>
								</div>
								<div className='col-12 col-md-6 col-xl-12'>
									<div className='contact-item'>
										<div className='contact-icon'>
											<i className='lni lni-phone'></i>
										</div>
										<div className='contact-content'>
											<h4>Связаться</h4>
											<p>{content.email}</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className='col-xl-8'>
						<div className='contact-form-wrapper'>
							{!showFormSuccess && (
								<FormikProvider value={formik}>
									<Form
										autoComplete='off'
										noValidate
										onSubmit={handleSubmit}
										className='contact-form'
										data-aos='fade-up'
									>
										<div className='row'>
											<div className='col-md-6'>
												<div className='input-container'>
													<input
														type='text'
														className={`form-control${
															errors.name ? ' is-invalid' : ''
														}`}
														id='name'
														placeholder='Ваше имя'
														required
														{...getFieldProps('name')}
													/>
													{errors.name && (
														<div className='invalid-feedback ms-2'>{errors.name}</div>
													)}
												</div>
											</div>
											<div className='col-md-6'>
												<div className='input-container'>
													<input
														type='email'
														className={`form-control${
															errors.email ? ' is-invalid' : ''
														}`}
														id='email'
														placeholder='Ваш Email'
														required
														{...getFieldProps('email')}
													/>
													{errors.email && (
														<div className='invalid-feedback ms-2'>
															{errors.email}
														</div>
													)}
												</div>
											</div>
										</div>
										<div className='row'>
											<div className='col-12'>
												<div className='input-container'>
													<textarea
														className={`form-control${
															errors.message ? ' is-invalid' : ''
														}`}
														rows={5}
														placeholder='Введите сообщение'
														required
														{...getFieldProps('message')}
													></textarea>
													{errors.message && (
														<div className='invalid-feedback ms-2'>
															{errors.message}
														</div>
													)}
												</div>
											</div>
										</div>
										<div className='row'>
											<div className='col-12'>
												<div className='button text-center rounded-buttons'>
													<button
														type='submit'
														className='btn primary-btn rounded-full'
													>
														Отправить
													</button>
												</div>
											</div>
										</div>
									</Form>
								</FormikProvider>
							)}
							{showFormSuccess && (
								<div className='my-3' data-aos='fade-up'>
									<div className='sent-message'>
										<h4>Ваше сообщение отправлено.</h4>
										<h4>Мы скоро свяжемся с вами.</h4>
									</div>
								</div>
							)}
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default Contacts;
