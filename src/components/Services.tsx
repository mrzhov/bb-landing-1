import type { FC } from 'react';

import content from '@/content/advantages.json';

const Services: FC = () => {
	return (
		<section id='advantages' className='services-area services-eight'>
			<div className='section-title-five'>
				<div className='container'>
					<div className='row'>
						<div className='col-12'>
							<div className='content'>
								<h6>{content.sectionName}</h6>
								<h2 className='fw-bold'>{content.title}</h2>
								<p>{content.subtitle}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className='container'>
				<div className='row'>
					{content.items.map((item) => (
						<div className='col-lg-4 col-md-6' key={item.title}>
							<div className='single-services'>
								<div className='service-icon'>
									<i className={`lni ${item.icon}`}></i>
								</div>
								<div className='service-content'>
									<h4>{item.title}</h4>
									<p>{item.subtitle}</p>
								</div>
							</div>
						</div>
					))}
				</div>
			</div>
		</section>
	);
};

export default Services;
