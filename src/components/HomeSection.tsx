import type { FC } from 'react';

import content from '@/content/home.json';

const HomeSection: FC = () => {
	return (
		<section id='hero-area' className='header-area header-eight'>
			<div className='container'>
				<div className='row align-items-center'>
					<div className='col-lg-6 col-md-12 col-12'>
						<div className='header-content'>
							<h1>{content.title}</h1>
							<p>{content.subtitle}</p>
						</div>
					</div>
					<div className='col-lg-6 col-md-12 col-12'>
						<div className='header-image'>
							<img src='images/home-img.jpg' alt='#' />
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default HomeSection;
