import type { FC } from 'react';

import content from '@/content/cta.json';

const Cta: FC = () => {
	return (
		<section id='call-action' className='call-action'>
			<div className='container'>
				<div className='row justify-content-center'>
					<div className='col-xxl-6 col-xl-7 col-lg-8 col-md-9'>
						<div className='inner-content'>
							<h2>{content.title}</h2>
							<p>{content.subtitle}</p>
							<div className='light-rounded-buttons'>
								<a href='#contacts' className='btn primary-btn-outline page-scroll-btn'>
									Связаться
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default Cta;
