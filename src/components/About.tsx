import type { FC } from 'react';

import content from '@/content/about.json';
import DotsSvg from '@/public/images/about/about-dots.svg';

const About: FC = () => {
	return (
		<section id='about' className='about-area about-five'>
			<div className='container'>
				<div className='row align-items-center'>
					<div className='col-lg-6 col-12'>
						<div className='about-image-five'>
							<DotsSvg className='shape' />
							<img src='images/about/about-img.jpg' alt='about' />
						</div>
					</div>
					<div className='col-lg-6 col-12'>
						<div className='about-five-content'>
							<h6 className='small-title text-lg'>{content.sectionName}</h6>
							<h2 className='main-title fw-bold'>{content.title}</h2>
							<div className='about-five-tab'>
								<div className='tab-content' id='nav-tabContent'>
									<div
										className='tab-pane fade show active'
										id='nav-who'
										role='tabpanel'
										aria-labelledby='nav-who-tab'
									>
										{content.subtitles.map((s) => (
											<p key={s}>{s}</p>
										))}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default About;
