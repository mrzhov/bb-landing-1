import Image from 'next/image';
import type { FC } from 'react';

import content from '@/content/clients.json';

const Clients: FC = () => {
	return (
		<div id='clients' className='brand-area section'>
			<div className='section-title-five mb-0'>
				<div className='container'>
					<div className='row'>
						<div className='col-12'>
							<div className='content'>
								<h6>{content.sectionName}</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className='container'>
				<div className='row'>
					<div className='col-12 d-flex justify-content-center'>
						<div className='clients-logos'>
							{content.items.map((item) => (
								<div className='single-image' key={item}>
									<Image
										src={`/images/clients/${item}`}
										priority
										width={170}
										height={90}
										alt='Brand Logo Images'
									/>
								</div>
							))}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Clients;
