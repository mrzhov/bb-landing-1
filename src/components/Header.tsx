import Link from 'next/link';
import type { FC } from 'react';
import { useState } from 'react';

import content from '@/content/header.json';

const Header: FC = () => {
	const [showMobileHeader, setShowMobileHeader] = useState<boolean>(false);

	return (
		<section className='navbar-area navbar-nine'>
			<div className='container'>
				<div className='row'>
					<div className='col-lg-12'>
						<nav className='navbar navbar-expand-lg'>
							<Link href='/'>
								<a className='navbar-brand me-auto me-lg-0'>
									<img src='/images/logo.jpg' alt='Logo' />
								</a>
							</Link>
							<button
								id='navbar-toggler'
								className={`navbar-toggler${showMobileHeader ? ' active' : ''}`}
								type='button'
								data-bs-toggle='collapse'
								data-bs-target='#navbarNine'
								aria-controls='navbarNine'
								aria-expanded='false'
								aria-label='Toggle navigation'
								onClick={() => setShowMobileHeader((prev) => !prev)}
							>
								<span className='toggler-icon'></span>
								<span className='toggler-icon'></span>
								<span className='toggler-icon'></span>
							</button>

							<div
								className={`navbar-collapse sub-menu-bar${
									showMobileHeader ? '' : ' collapse'
								}`}
								id='navbarNine'
							>
								<ul className='navbar-nav ms-auto'>
									{content.map((el) => (
										<li className='nav-item' key={el.href}>
											<a className='page-scroll' href={el.href}>
												{el.title}
											</a>
										</li>
									))}
								</ul>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</section>
	);
};

export default Header;
