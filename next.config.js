const nextConfig = {
	reactStrictMode: true,
	experimental: {
		outputStandalone: true,
	},
	webpack(config) {
		config.module.rules.push({
			test: /\.svg$/,
			use: ['@svgr/webpack'],
		});

		return config;
	},
};

module.exports = nextConfig;
