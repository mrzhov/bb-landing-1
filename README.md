## Run in dev mode

1. `yarn install`
1. `yarn run dev`

## Build project using Docker

1. Build your container: `docker build -t nextjs-docker .`
1. Run your container: `docker run -d -p 80:80 nextjs-docker`

### Icons
```
https://lineicons.com/icons/
```
